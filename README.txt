PeerDeviceNet: Secure Ad-Hoc Peer-Peer Communication Among Computing Devices

src/PeerDeviceNet_Core:
	PeerDeviceNet(Core) runtime, an android application project.

src/zxing_client_latest:
	a customized subset of ZXing's android client code, an android library project which PeerDeviceNet_Core depends on; used by PeerDeviceNet to implement connecting peer devices by scanning QR code.

samples/PeerDeviceNet_Chat:
	a sample chat android app project, demonstrating using PeerDeviceNet's three kinds of APIs (idl, messenger,intents).
 